@extends('layouts.default') @section('content')
  <section>
  <div class="container">
  <div class="bg-white py-8 px-6">
    <div class="bg-grey-light text-center rounded">
            <img src="img/group-logo.png" alt="Group Logo">
    </div>
      <div class="fleet-form-error">

                 @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            @include('partials.flashmessage')

          </div>

         <div class="px-3 w-full flex">
  <h2 class="py-6">Meet & Greet</h2>
      </div>

  <form method="POST" action="/fleet-form">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="flex flex-wrap mb-6">
  <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
              <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2"> Sales Executive:</label>
              <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="sales_executive" type="text" value="" required="true">

             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">Customer Name:</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="customer_name" type="text" value="" required="true">

             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">Customer Phone Number:</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="customer_telephone" type="text" value="" required="true">






             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">Customer Email Address:</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="customer_email" type="text" value="" required="true">

        </div>

        <div class="w-1/2 px-2">

             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">First Line of Address:</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="customer_address" type="text" value="" required="true">


              <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2"> Town:</label>
              <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="customer_town" type="text" value="" required="true">



             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">County:</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="customer_county"  type="text" value="" required="true">



             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">Postcode:</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="customer_postcode"  type="text" value="" required="true">

        </div>

        </div>

      <div class="flex">
                <div class="w-1/2 px-2">
                      <div>
         <div>

             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">Company Name:</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="company_name" type="text" value="">

      </div>
      </div>

         <div>

             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">Decision maker / Fleet Manager name:</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="company_manager" type="text" value="">

      </div>
                  <div>


             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">Business Type:</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="company_type" type="text" value="">
      </div>

          </div>
                  <div class="w-1/2 px-2">
                      <div>
         <div>

             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">Fleet Size:</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="company_size" type="text" value="">
      </div>

      </div>
                      <div>


             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">Prefered funder / leasing company if known?</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="company_leasing"  type="text" value="">

      </div>

         <div>

             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">Fuel Policy:</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="company_policy"  type="text" value="">
      </div>


      </div>
          </div>
        <div class="w-1/2 px-2">
      <h2 class="py-6">Qualification</h2>
      </div>

          <div class="flex">
              <div class="w-1/2 px-2">
                      <div>


             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">Model of interest:</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="car_model"  type="text" value="">



      </div>

         <div>

             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">Have they configured a car online?</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="car_configured"  type="text" value="">

      </div>

                      <div>


             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">If yes - does the customer have the "Short Code"?</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="car_shortcode"  type="text" value="">


      </div>
                                    <div>


             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">What will you be using the new car for?</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="car_usage" type="text" value="">


      </div>
      </div>

                      <div class="w-1/2 px-2">
                      <div>


             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">What car do you currently drive?</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="car_current"  type="text" value="">
      </div>

                      <div>

             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">What do you like about your current car?</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="car_interested" type="text" value="">

      </div>
                      <div>

             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">What do you dislike about your current car?</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight"name="car_dislike"  type="text" value="">

      </div>

                                            <div>

             <label class="block uppercase tracking-wide text-grey-darker text-sm font-bold mb-2">Are there any "must haves" in terms of your new car?</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="car_must_have"  type="text" value="">

      </div>

          </div>
      </div>

        <div class="w-full px-2 pb-6 flex flex-wrap">
              <h2 class="py-6">Finance</h2>

                                      <div class="w-full">
                                  <h3 class="tracking-wide text-grey-darker font-bold mb-2">Please reference accessories that may interest the customer.</h3>
              </div>
                      <div class="w-full">

                     <label class="tracking-wide text-grey-darker text-lg font-bold mb-2">How does the customer intend to fund the vehicle?</label>
             <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" name="car_fund" type="text" value="">
             <p class="text-grey-darker font-bold mb-2">Visit Pulman Fleet for a full explanation of our finance offers...<a href="http://pulmangroup.co.uk/fleet/learn-about-contract-hire" target="_blank">Learn more</a></p>
                                                            <div>
                                 <div class="w-full">
                                  <h3 class="text-grey-darker font-bold mb-2">Provide a full PCP quotation and explain to the customer finance options.</h3>
                                      </div>


              </div>


      </div>

          </div>


          <div class="w-full px-2">
              <h2 class="pb-6">Presentation & Demonstration</h2>
          </div>

          <div class="w-full px-2 flex flex-wrap">

                                      <div class="w-full">
                                          <label class="text-grey-darker font-bold mb-2">
                                            <input type="hidden"  name="static_walkaround" value="No" />
                                            <input type="checkbox" value="Yes" name="static_walkaround"> Perform a static walkaround of the vehicle</label>
  </div>
  <div class="w-full">
    <label class="text-grey-darker font-bold mb-2">
  <input type="hidden"  name="sit_in" value="No" />
      <input type="checkbox" value="Yes" name="sit_in"> Offer the customer the opportunity to sit in the vehicle
    </label>
  </div>
  <div class="w-full">
    <input type="hidden"  name="key_features" value="No" />
    <label class="text-grey-darker font-bold mb-2">
      <input type="checkbox" value="Yes" name="key_features">
      Demonstrate the key features / must have features</label>
  </div>
                                      <div class="w-full">
                                        <input type="hidden"  name="safety_rating" value="No" />
    <label class="text-grey-darker font-bold mb-2"><input type="checkbox" value="Yes" name="safety_rating"> Confirm NCAP safety rating</label>
  </div>

                                                  <div class="w-full">
                                                    <input type="hidden"  name="test_drive" value="No" />
    <label class="text-grey-darker font-bold mb-2">
      <input type="checkbox" value="Yes"name="test_drive"> Offer a test drive</label>
  </div>

                                                                                   <div>
                                 <div class="w-full md:w-2/3 lg:w-full">
                                  <h3 class="text-grey-darker font-bold mb-2 py-4">Use the iPad videos to demonstrate product features and benefits</h3>
                                      </div>


              </div>
                              <!-- Contact Message -->
                              <div class="w-full">
  <label class="tracking-wide text-grey-darker text-sm font-bold mb-2">What did the customer think of the car after the test drive?</label>
                                  <div>
                                    <input type="hidden"  name="test_drive_feedback" value="No" />
                                      <textarea class="resize-none appearance-none block w-full bg-grey-lighter text-grey-darker border  rounded py-4 px-4 mb-3 leading-tight" rows="10" name="test_drive_feedback"></textarea>
                                  </div>

                              </div><!-- /Contact Message -->

                             <div class="tool-tip">
                             <div class="w-full md:w-2/3 lg:w-full mb-2">
                                  <h3 class="tracking-wide text-grey-darker text-sm font-bold">Build desire into the product!</h3>
                                      </div>
                                    </div>
                                  </div>


          <div class="w-full px-2">
              <h2 class="py-6">Proposition  & gaining an agreement</h2>
          </div>

          <div class="flex">
          <div class="w-full px-2">

                                      <div>
                                          <label class="text-grey-darker font-bold mb-2">
  <input type="hidden"  name="key_requirements" value="No" />
                                            <input type="checkbox" value="Yes" name="key_requirements"> Summarise customers key requirements</label>
  </div>
  <div class="">
    <label class="text-grey-darker font-bold mb-2">
  <input type="hidden"  name="close_sale" value="No" />
      <input type="checkbox" value="Yes" name="close_sale"> Attempt to close the sale</label>
  </div>
  <div class="">
    <label class="text-grey-darker font-bold mb-2">
      <input type="hidden"  name="vw_service_plans" value="No" />
      <input type="checkbox" value="Yes" name="vw_service_plans"> Explain VWFS service plans</label>
  </div>

          <div class="">
    <label class="text-grey-darker font-bold mb-2">
  <input type="hidden"  name="reinforce_brand" value="No" />
    <input type="checkbox" value="Yes" name="reinforce_brand"> Reinforce Volkswagen brand</label>
  </div>

       </div>
      </div>

              <div class="w-full px-2">
              <h2 class="py-6">Follow up</h2>
          </div>

          <div class="flex">
          <div class="w-full px-2">

                                      <div>
                                          <label class="text-grey-darker font-bold mb-2">
                                            <input type="hidden"  name="agree_next_step" value="No" />
                                            <input type="checkbox" name="agree_next_step" value="Yes"> Agree the next step - followup, handover to fleet team?</label>
  </div>
  <div>
    <label class="text-grey-darker font-bold mb-2">
        <input type="hidden"  name="data_vlm" value="No" />
      <input type="checkbox" value="Yes" name="data_vlm"> Enter customer data into VLM</label>
  </div>
       </div>
      </div>

      <div class="flex w-full my-4 px-3">

          <button  type="submit" value="Submit" class="w-1/5 py-4 bg-blue text-white rounded">Submit</button>

      </div>

      </form>

  </div>
  </section>


@stop
