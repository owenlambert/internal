@extends('layouts.default') @section('content')
<div id="app">

<header class="header">
      <div class="wrap">
          <div class="row center-xs">
              <div class="col-xs-12">
                <img src="img/group-logo.png" alt="Group Logo">
                <h1>Welcome to the Pulman Phone Directory.</h1>
                <p>We will be updating our phone line system which will overhaul all of the extensions numbers across the group shortly.</p>
                <p>You can find all of the up to date extensions numbers below.</p>
                <a href="#Directory" class="btn btn-green">Start Your Search</a>
              </div>
          </div>
          </div>
</header>

<section class="grey-content-wrapper" id="Directory">
        <div class="wrap">
            <div class="row center-xs">
                <div class="col-xs-12">

                  <div class="heading">
                    <h2>Directory Data</h2>
                    <span class="sub">Please search our new directory below:</span>
                  </div>

              <div class="panel panel-white">
              <div class="panel-body">
                  <div id="no-more-tables">
                      <v-client-table :data="tableData" :columns="columns" :options="options"></v-client-table>
                  </div>
                     @if (Auth::check())
                     <span>Hello, {{ Auth::user()->email}}</span>
                     <form style="margin-top:10px;"action="{{ URL::to('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <label class="btn btn-blue btn-file">
                      <input type="file" name="file" />
                    </label>
                      <button class="btn btn-green-small" style="height: 39px;">Import File</button>
                    </form>
                      @else
                      @endif

                  @include('partials.flashmessage')
                </div>
                </div>
              </div>
            </div>
        </div>
</section>

@endsection @section('scripts')
@endsection
