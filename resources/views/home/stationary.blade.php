@extends('layouts.default')
@section('content')

      <section class="Email-Stationary">
        <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="Email-Stationary-Wrapper">
              <div class="row">
                <div class="col-md-4">
                  <h1>Volkswagen Sales Durham</h1>
                  <img src="http://pulmangroup.co.uk/Email/VW-Sales-Durham.jpg" alt="">
                </div>
                <div class="col-md-4">
                  <h1>Volkswagen Sales Sunderland</h1>
                  <img src="http://pulmangroup.co.uk/Email/VW-Sales-Sunderland.jpg" alt="">
                </div>
                <div class="col-md-4">
                  <h1>SKODA Sales Durham</h1>
                  <img src="http://pulmangroup.co.uk/Email/SKODA-Sales.jpg" alt="">
                </div>
              </div>
            </div>

            <div class="Email-Stationary-Wrapper">
              <div class="row">
                <div class="col-md-4">
                  <h1>SEAT Sales Sunderland</h1>
                  <img src=" http://pulmangroup.co.uk/Email/SEAT-Sales.jpg" alt="">
                </div>
                <div class="col-md-4">
                  <h1>Pulman Marketing</h1>
                  <img src="http://pulmangroup.co.uk/Email/Group.jpg" alt="">
                </div>
                <div class="col-md-4">
                  <h1>Senior Managers</h1>
                  <img src="http://pulmangroup.co.uk/Email/Group.jpg" alt="">
                </div>
              </div>
            </div>

            <div class="Email-Stationary-Wrapper">
              <div class="row">
                <div class="col-md-4">
                  <h1> Volkswagen Durham Service</h1>
                  <img src="http://pulmangroup.co.uk/Email/VW-Servicing-Durham.jpg" alt="">
                </div>
                <div class="col-md-4">
                  <h1>Volkswagen Sunderland Service</h1>
                  <img src="http://pulmangroup.co.uk/Email/VW-Servicing-Sunderland.jpg" alt="">
                </div>
                <div class="col-md-4">
                  <h1>SEAT Sunderland Service</h1>
                  <img src="http://pulmangroup.co.uk/Email/SEAT-Servicing.jpg" alt="">
                </div>
              </div>
            </div>

            <div class="Email-Stationary-Wrapper">
              <div class="row">
                <div class="col-md-4">
                  <h1>Pulman Contact Centre</h1>
                  <img src="http://pulmangroup.co.uk/Email/ContactCentre.jpg" alt="">
                </div>
                <div class="col-md-4">
                  <h1>Pulman Accounts</h1>
                  <img src="http://pulmangroup.co.uk/Email/Group.jpg" alt="">
                </div>
                <div class="col-md-4">
                  <h1>Pulman HR</h1>
                  <img src="http://pulmangroup.co.uk/Email/Group.jpg" alt="">
                </div>
              </div>
            </div>

            <div class="Email-Stationary-Wrapper">
              <div class="row">
                <div class="col-md-4">
                  <h1>Pulman Fleet SEAT </h1>
                  <img src="http://pulmangroup.co.uk/Email/SEAT-Fleet.jpg" alt="">
                </div>
                <div class="col-md-4">
                  <h1>Pulman Fleet VW</h1>
                  <img src="http://pulmangroup.co.uk/Email/VW-Fleet.jpg" alt="">
                </div>
                <div class="col-md-4">
                  <h1>Pulman Fleet SKODA</h1>
                  <img src="http://pulmangroup.co.uk/Email/SKODA-Fleet.jpg" alt="">
                </div>
              </div>
            </div>

            <div class="Email-Stationary-Wrapper">
              <div class="row">
                <div class="col-md-4">
                  <h1>Pulman Sales Admin </h1>
                  <img src="http://pulmangroup.co.uk/Email/Group.jpg" alt="">
                </div>
                <div class="col-md-4">
                  <h1>Pulman Fleet Group</h1>
                  <img src="http://pulmangroup.co.uk/Email/Fleet-General.jpg" alt="">
                </div>
                <div class="col-md-4">
                  <h1>Pulman Skoda Service</h1>
                  <img src="http://www.pulmangroup.co.uk/Email/SKODA-Sales-Coming-Soon.jpg" alt="">
                </div>
              </div>
            </div>

            <div class="Email-Stationary-Wrapper Last-Item">
              <div class="row">
                <div class="col-md-4">
                  <h1>Pulman VW Durham Parts</h1>
                  <img src=" http://pulmangroup.co.uk/Email/Parts-Stationary.jpg" alt="">
                </div>
                <div class="col-md-4">
                  <h1>Pulman VW Sunderland Parts</h1>
                  <img src="http://pulmangroup.co.uk/Email/Parts-Stationary.jpg" alt="">
                </div>
                <div class="col-md-4">
                  <h1>Pulman Skoda Parts</h1>
                  <img src=" http://www.pulmangroup.co.uk/Email/SKODA.jpg" alt="">
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      </section>

@endsection
@section('scripts')
@endsection
