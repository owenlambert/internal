<!DOCTYPE html>

<html lang="en">
<head>

<title>Pulman Group - Phone Directory</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="x-ua-compatible" content="IE=10" >
<link rel="stylesheet" href="css/all.css?v=1.0">
<link rel="stylesheet" href="css/vendor/bootstrap.css">
<link href='https://fonts.googleapis.com/css?family=Lato:400,100,300,900,700' rel='stylesheet' type='text/css'>

  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

</head>

<body>

  @yield('content')
  <script src="{{ asset ("js/app.js") }}"></script>
  @yield('scripts')
</body>

</html>
