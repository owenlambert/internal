$('.hamburger').on('click', function() {
  if ($('nav').hasClass('hide')) {
    $('nav').removeClass('hide');
    $('nav').slideDown();
  } else {
    $('nav').addClass('hide');
  }
});
