<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFleetFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleet_form', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sales_executive');
            $table->string('customer_name');
            $table->string('customer_telephone');
            $table->string('customer_email');
            $table->string('customer_address');
            $table->string('customer_town');
            $table->string('customer_county');
            $table->string('customer_postcode');
            $table->string('company_name');
            $table->string('company_manager');
            $table->string('company_type');
            $table->string('company_size');
            $table->string('company_leasing');
            $table->string('company_policy');
            $table->string('car_model');
            $table->string('car_configured');
            $table->string('car_shortcode');
            $table->string('car_usage');
            $table->string('car_current');
            $table->string('car_interested');
            $table->string('car_dislike');
            $table->string('car_must_have');
            $table->string('car_fund');
            $table->string('static_walkaround');
            $table->string('sit_in');
            $table->string('key_features');
            $table->string('safety_rating');
            $table->string('test_drive');
            $table->text('test_drive_feedback');
            $table->string('key_requirements');
            $table->string('close_sale');
            $table->string('vw_service_plans');
            $table->string('reinforce_brand');
            $table->string('agree_next_step');
            $table->string('data_vlm');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fleet_form');
    }
}
