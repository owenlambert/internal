<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Directory extends Model
{
  protected $table = 'intranet_directory';

  protected $fillable = [
   'department',
   'firstName',
   'lastName',
   'email',
   'title',
   'extension'
   ];
}
