<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FleetForm extends Model
{
    //
    protected $fillable = [
      'sales_executive',
      'customer_name',
      'customer_telephone',
      'customer_email',
      'customer_address',
      'customer_town',
      'customer_county',
      'customer_postcode',
      'company_name',
      'company_manager',
      'company_type',
      'company_size',
      'company_leasing',
      'company_policy',
      'car_model',
      'car_configured',
      'car_shortcode',
      'car_usage',
      'car_current',
      'car_interested',
      'car_dislike',
      'car_must_have',
      'car_fund',
      'static_walkaround'.
      'sit_in',
      'key_features',
      'safety_rating',
      'test_drive',
      'test_drive_feedback',
      'key_requirements',
      'close_sale',
      'vw_service_plans',
      'reinforce_brand',
      'agree_next_step',
      'data_vlm'
      // add all other fields
  ];
  protected $table = 'fleet_form';

}
