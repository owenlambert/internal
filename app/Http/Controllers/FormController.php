<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\FleetForm;
use Mail;
use App\Mail\FleetMail;

  class FormController extends Controller
{
    public function index()
    {
        return view('home.form');
    }
public function store(Request $request){
Fleetform::create($request->all());
$data = $request->all();

Mail::to('enquiries@pulmangroup.co.uk')->send(new FleetMail($data));

return back();
}
}
