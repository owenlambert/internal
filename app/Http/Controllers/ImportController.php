<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Directory;
use App\Http\Requests\DirectoryImportRequest;
use Flash;
use DB;
use Excel;

class ImportController extends Controller
{

    public function importDirectoryList(DirectoryImportRequest $request)
  {
    if($request->hasFile('file')){
      $data = Excel::load($request->file('file'), function($reader) {
      })->get();
      if(!empty($data) && $data->count()){
        foreach ($data as $key => $value) {
          if(!empty($value->licenses) & !empty($value->phonenumber)) {
        $insert[] = ['firstname' => $value->firstname, 'lastname' => $value->lastname, 'department' => $value->department, 'email' => $value->userprincipalname, 'extension' => $value->phonenumber];
        }
       }
        if(!empty($insert)){
          DB::table('intranet_directory')->truncate();
          DB::table('intranet_directory')->insert($insert);
          Flash::success('Sucessfully added data to the directory');
          return redirect()->back();
        }
      }
    }
    return redirect()->back();
  }




}
