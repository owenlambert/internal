<?php

namespace App\Http\Controllers;

use App\Models\Directory;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.index');
    }

    public function data()
    {
        $data = Directory::latest()->get();

        return response()->json($data);
    }
}
