<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
Route::get('/api/directory', 'HomeController@data');
Route::post('importExcel', 'ImportController@importDirectoryList');
Route::get('/fleet-form', 'FormController@index');
Route::post('/fleet-form', 'FormController@store');
