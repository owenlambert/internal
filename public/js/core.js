Vue.use(VueTables.client, {
    highlightMatches: true,
    filterByColumn: true,
    texts: {
        filter: "Search:"
    },
    datepickerOptions: {
        showDropdowns: true
    }
});

new Vue({
  el:"#app",
  created: function() {
    this.fetchDirectory();
  },
  methods: {
    fetchDirectory: function() {
    this.$http.get('/api/directory')
     .then(function(response){
     this.tableData = response.data
     console.log(this.tableData);
     }.bind(this))
   },
  },
  data: {
    columns:['department','firstName', 'lastName','email', 'extension'],
    options: {
    headings: {
          firstName: 'First Name',
          lastName: 'Last Name',
          email: 'Email',
          title: 'Title',
          extension: 'Extension'
      },
      listColumns:{
        department:[
        {value:'Marketing & Communications',text:'Marketing & Communications'},
        {value:'Sales',text:'Sales'},
        {value:'Workshop',text:'Workshop'},
        {value:'Contact Centre',text:'Contact Centre'},
        {value:'HR',text:'HR'},
        {value:'Accounts',text:'Accounts'},
        {value:'Parts',text:'Parts'},
        {value:'Cafe',text:'Cafe'},
        {value:'Aftersales',text:'Aftersales'},
        {value:'Admin',text:'Admin'},
        {value:'Conference Room',text:'Conference Room'},
        ]
      },
        },
    tableData: [this.tableData]
},
});

$(document).ready(function(){
$('a[href^="#"]').on('click',function (e) {
  e.preventDefault();

  var target = this.hash;
  var $target = $(target);

  $('html, body').stop().animate({
      'scrollTop': $target.offset().top
  }, 900, 'swing', function () {
      window.location.hash = target;
  });
});
});
