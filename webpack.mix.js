let mix = require('laravel-mix');
var tailwindcss = require('tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.sass('resources/assets/sass/app.scss', 'public/css')
  .options({
    processCssUrls: false,
    postCss: [
      tailwindcss('./tailwind.js'),
    ]
  });
  mix.scripts([
    "public/js/vendor/jQuery.min.js",
    "public/js/vendor/vue.js",
    "public/js/vendor/vue-resource.min.js",
    "public/js/vendor/vuetables.min.js",
    "public/js/core.js"
 ],'public/js/app.js');

 mix.styles([
   'public/css/app.css',
   'resources/assets/sass/vendor/font-awesome.min.css',
   'resources/assets/sass/vendor/flexboxgrid.min.css',
   'resources/assets/sass/vendor/normalize.css',
 ],'public/css/all.css');
